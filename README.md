# Twig string renderer for Yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Run

```
composer require --prefer-dist kalmer/yii2-twig-stringrenderer
```

## Usage

Either use directly in your class:

```php
use kalmer\twig\StringRenderer;

class YourClassName extends BaseObject
{
    public function yourMethod()
    {
        $renderer = new StringRenderer();

        $renderer->render('your twig {{ template_name }}', [
            'template_name' => 'template'
        ]);
    }
}
```

or put following snippet into your config:

```php
return [
    'components' => [
        'twigRenderer' => [
            'class' => kalmer\twig\StringRenderer::class,
        ]
    ]
];
```

And use the component in your class:

```php
class YourClassName extends BaseObject
{
    public function yourMethod()
    {
        Yii::$app->twigRenderer->render('your twig {{ template_name }}', [
            'template_name' => 'template'
        ]);
    }
}
