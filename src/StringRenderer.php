<?php

namespace kalmer\twig;

use Twig\Environment;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\twig\TwigEmptyLoader;

class StringRenderer extends Component
{
    /**
     * @var string the directory or path alias pointing to where Twig cache will be stored. Set to false to disable
     * templates cache.
     */
    public $cachePath = '@runtime/Twig/cache';

    /**
     * @var array Twig options.
     * @see http://twig.symfony.com/doc/api.html#environment-options
     */
    public $options = [];

    /**
     * @var Environment twig environment object that renders twig templates
     */
    public $twig;

    public function init()
    {
        $loader = new TwigEmptyLoader();
        $this->twig = new Environment($loader, ArrayHelper::merge([
            'cache' => Yii::getAlias($this->cachePath),
            'charset' => Yii::$app->charset,
        ], $this->options));
    }

    public function render($template, $templateAttributes)
    {
        $templateWrapper = $this->twig->createTemplate($template);
        return $templateWrapper->render($templateAttributes);
    }
}
