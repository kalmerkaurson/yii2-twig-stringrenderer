<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../vendor/autoload.php';

Yii::setAlias('@tests', __DIR__);

$config = [
    'id' => 'app-test',
    'basePath' => dirname(__DIR__),
];

(new yii\web\Application($config));
