<?php

namespace tests\unit;

use Codeception\Test\Unit;
use kalmer\twig\StringRenderer;
use Twig\Error\SyntaxError;

class TwigValidatorTest extends Unit
{
    public function testRenderParameters()
    {
        $renderer = new StringRenderer([
            'cachePath' => '@tests/runtime/Twig/cache',
        ]);

        $this->assertEquals('test test_template', $renderer->render('test {{ template_name }}', [
            'template_name' => 'test_template',
        ]));
    }

    public function testRenderInvalidTwig()
    {
        $renderer = new StringRenderer([
            'cachePath' => '@tests/runtime/Twig/cache',
        ]);

        $this->expectException(SyntaxError::class);

        $renderer->render('test {{ template_name}', [
            'template_name' => 'test_template',
        ]);
    }
}
